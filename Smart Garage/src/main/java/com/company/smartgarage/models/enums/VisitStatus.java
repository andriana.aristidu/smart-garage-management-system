package com.company.smartgarage.models.enums;

public enum VisitStatus {
    OPEN, CLOSED
}

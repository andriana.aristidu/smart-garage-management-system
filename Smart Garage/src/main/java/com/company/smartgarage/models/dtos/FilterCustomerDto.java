package com.company.smartgarage.models.dtos;

import lombok.Data;
@Data
public class FilterCustomerDto {

    private String licencePlate;
    private String localDate;
}

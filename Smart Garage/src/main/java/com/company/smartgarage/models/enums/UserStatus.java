package com.company.smartgarage.models.enums;

public enum UserStatus {
    ACTIVE, INACTIVE
}

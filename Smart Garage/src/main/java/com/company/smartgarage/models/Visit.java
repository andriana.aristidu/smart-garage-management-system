package com.company.smartgarage.models;

import com.company.smartgarage.models.enums.Currency;
import com.company.smartgarage.models.enums.VisitStatus;
import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@Table(name = "visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "visit_id")
    private int id;
    @Column(name = "visit_date")
    private LocalDate creationDate;
    @Column(name = "total_price")
    private double totalPrice;
    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "currency")
    private Currency currency;
    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "status")
    private VisitStatus status;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "visit_id")
    private Set<DoneService> services;
    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;
}

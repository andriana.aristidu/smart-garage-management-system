package com.company.smartgarage.repositories.contracts;

import com.company.smartgarage.models.MaintenanceCategory;

public interface MaintenanceCategoryRepository extends BaseCrudRepository<MaintenanceCategory> {
}

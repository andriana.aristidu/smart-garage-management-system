
INSERT INTO smart_garage.users (user_id, username, password, email, phone_number, user_role, user_names)
VALUES (1, 'mitaka', 'qwe123EWQ!', 'mitaka@gmail.com', '0897199999', 'ADMINISTRATOR', 'Mitko Chernev');
INSERT INTO smart_garage.users (user_id, username, password, email, phone_number, user_role, user_names)
VALUES (2, 'peshaka', 'QWE123ewq!', 'peshaka@gmail.com', '0897199998', 'EMPLOYEE', 'Pesho Petrov');
INSERT INTO smart_garage.users (user_id, username, password, email, phone_number, user_role, user_names)
VALUES (3, 'divaka', 'QWe123ewq!!', 'divaka@gmail.com', '0897199997', 'EMPLOYEE', 'Dimo Divakov');
INSERT INTO smart_garage.users (user_id, username, password, email, phone_number, user_role, user_names)
VALUES (4, 'sergo', 'Sergo123!', 'sergoB@gmail.com', '0897121314', 'CUSTOMER', 'Sergey Borisovich');
INSERT INTO smart_garage.users (user_id, username, password, email, phone_number, user_role, user_names)
VALUES (5, 'ddonchev', 'ddoncheV123!', 'dondonchev@gmail.com', '0897333333', 'CUSTOMER', 'Doncho Donchev');
INSERT INTO smart_garage.users (user_id, username, password, email, phone_number, user_role, user_names)
VALUES (6, 'madjo', 'mmaaDD123!', 'madjoo@gmail.com', '0897331133', 'CUSTOMER', 'Mladen Mihalev');


INSERT INTO smart_garage.vehicles (vehicle_id, license_plate, vin, creation_year, model_id, user_id)
VALUES (1, 'CB4456CT', '12345123451234567', 2020, 2, 4);
INSERT INTO smart_garage.vehicles (vehicle_id, license_plate, vin, creation_year, model_id, user_id)
VALUES (2, 'A1234AB', '12345612345612345', 2004, 1, 5);
INSERT INTO smart_garage.vehicles (vehicle_id, license_plate, vin, creation_year, model_id, user_id)
VALUES (3, 'Y9993HM', '12345678912345678', 2013, 8, 6);



